var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/school', function(err, db) {
    if(err) throw err;

    // var query = {};
    // var operator = {{'$unwind':'$scores'}, {'$match' : {'scores.type':'homework'}}, {'$group':{'_id': '$_id', 'score': {'$min': '$scores.score'}}}};
    // var options = { '$sort': {'_id':1} };
    				

    db.collection('students').aggregate([{'$unwind':'$scores'}, 
                                        {'$match' : {'scores.type':'homework'}}, 
                                        {'$group':{'_id': '$_id', 'score': {'$min': '$scores.score'}}}, 
                                        {'$sort': {'_id':1}}], {}, 
                                        function(err, result) {
                                                                               //console.dir(result);   

                                                                result.forEach( function(doc){

                                                                 query = {'_id':doc._id};
                                                                 operator = {'$pull':{'scores':{'score':doc.score}}};
                                                                 db.collection('students').update(query, operator, function(err, updated){
                                                                      if(err) throw err;
                                                                      console.dir("Successfully updated "+ updated + "documents!");

                                                                      }); 
    


                                                                })

                                                                                db.close();

                                                                });
    
        
});
